<?php

use App\Http\Controllers\CareersController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TemplateController;
use App\Http\Controllers\ContactFormController;
use App\Http\Controllers\FormController;
use SebastianBergmann\Template\Template;

// use TCG\Voyager\Voyager;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/**
 * homepage
 */
Route::get('/', 'App\Http\Controllers\TemplateController@index');

/**
 * all pages related to blogs
 * '/blogs' - main blog page
 * '/blogs/{slug}' -refers to each single blog page
 * '/blogs/tags/{tags}' -blog tags filter
 */
Route::get('/blogs', 'App\Http\Controllers\TemplateController@blogs');
Route::get('/blogs/{slug}', 'App\Http\Controllers\TemplateController@singleBlog');
Route::get('/blogs/tags/{tags}', 'App\Http\Controllers\TagsController@index');

Route::get('/project', 'App\Http\Controllers\TemplateController@project');
Route::get('/privacy-policy', 'App\Http\Controllers\TemplateController@privacy');
Route::get('/terms-and-conditions', 'App\Http\Controllers\TemplateController@terms');

/**
 * render Careers view individually and show all
 */
Route::get('/careers', [TemplateController::class, 'careersRend']);
Route::get('/careers/{title}', [TemplateController::class, 'eachCareerPos']);

/**
 * POST request for form submission, validation, and mailing
 */
Route::post('/form-submit', [FormController::class, 'applyFormPost']);

/**
 * refresh form captcha
 */
Route::get('/captcha-refresh', [FormController::class, 'reloadCaptcha']);






Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


/**
 * contact form show view
 */
Route::get('/contact', [FormController::class, 'contact']);

/**
 * contact form submit
 */

Route::post('/contact/form-submit', [FormController::class, 'contactFormPost'])->name('contact.store');

Route::fallback(function (){
    return view ('404');
});