<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BlogsTagsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('blogs_tags')->delete();
        
        \DB::table('blogs_tags')->insert(array (
            0 => 
            array (
                'blogs_id' => 1,
                'tags_id' => 4,
            ),
            1 => 
            array (
                'blogs_id' => 1,
                'tags_id' => 5,
            ),
            2 => 
            array (
                'blogs_id' => 2,
                'tags_id' => 2,
            ),
            3 => 
            array (
                'blogs_id' => 4,
                'tags_id' => 3,
            ),
            4 => 
            array (
                'blogs_id' => 5,
                'tags_id' => 1,
            ),
            5 => 
            array (
                'blogs_id' => 5,
                'tags_id' => 2,
            ),
        ));
        
        
    }
}