<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Models\Role;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'browse_admin',
                'table_name' => NULL,
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'browse_bread',
                'table_name' => NULL,
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'browse_database',
                'table_name' => NULL,
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'browse_media',
                'table_name' => NULL,
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'browse_compass',
                'table_name' => NULL,
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'browse_menus',
                'table_name' => 'menus',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'read_menus',
                'table_name' => 'menus',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'edit_menus',
                'table_name' => 'menus',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'add_menus',
                'table_name' => 'menus',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'delete_menus',
                'table_name' => 'menus',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'browse_roles',
                'table_name' => 'roles',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'read_roles',
                'table_name' => 'roles',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'edit_roles',
                'table_name' => 'roles',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            13 => 
            array (
                'id' => 14,
                'key' => 'add_roles',
                'table_name' => 'roles',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            14 => 
            array (
                'id' => 15,
                'key' => 'delete_roles',
                'table_name' => 'roles',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            15 => 
            array (
                'id' => 16,
                'key' => 'browse_users',
                'table_name' => 'users',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            16 => 
            array (
                'id' => 17,
                'key' => 'read_users',
                'table_name' => 'users',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            17 => 
            array (
                'id' => 18,
                'key' => 'edit_users',
                'table_name' => 'users',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            18 => 
            array (
                'id' => 19,
                'key' => 'add_users',
                'table_name' => 'users',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            19 => 
            array (
                'id' => 20,
                'key' => 'delete_users',
                'table_name' => 'users',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            20 => 
            array (
                'id' => 21,
                'key' => 'browse_settings',
                'table_name' => 'settings',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            21 => 
            array (
                'id' => 22,
                'key' => 'read_settings',
                'table_name' => 'settings',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            22 => 
            array (
                'id' => 23,
                'key' => 'edit_settings',
                'table_name' => 'settings',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            23 => 
            array (
                'id' => 24,
                'key' => 'add_settings',
                'table_name' => 'settings',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            24 => 
            array (
                'id' => 25,
                'key' => 'delete_settings',
                'table_name' => 'settings',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            25 => 
            array (
                'id' => 26,
                'key' => 'browse_categories',
                'table_name' => 'categories',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            26 => 
            array (
                'id' => 27,
                'key' => 'read_categories',
                'table_name' => 'categories',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            27 => 
            array (
                'id' => 28,
                'key' => 'edit_categories',
                'table_name' => 'categories',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            28 => 
            array (
                'id' => 29,
                'key' => 'add_categories',
                'table_name' => 'categories',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            29 => 
            array (
                'id' => 30,
                'key' => 'delete_categories',
                'table_name' => 'categories',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            30 => 
            array (
                'id' => 31,
                'key' => 'browse_posts',
                'table_name' => 'posts',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            31 => 
            array (
                'id' => 32,
                'key' => 'read_posts',
                'table_name' => 'posts',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            32 => 
            array (
                'id' => 33,
                'key' => 'edit_posts',
                'table_name' => 'posts',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            33 => 
            array (
                'id' => 34,
                'key' => 'add_posts',
                'table_name' => 'posts',
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
            34 => 
            array (
                'id' => 35,
                'key' => 'delete_posts',
                'table_name' => 'posts',
                'created_at' => '2021-10-07 01:25:33',
                'updated_at' => '2021-10-07 01:25:33',
            ),
            35 => 
            array (
                'id' => 36,
                'key' => 'browse_pages',
                'table_name' => 'pages',
                'created_at' => '2021-10-07 01:25:33',
                'updated_at' => '2021-10-07 01:25:33',
            ),
            36 => 
            array (
                'id' => 37,
                'key' => 'read_pages',
                'table_name' => 'pages',
                'created_at' => '2021-10-07 01:25:33',
                'updated_at' => '2021-10-07 01:25:33',
            ),
            37 => 
            array (
                'id' => 38,
                'key' => 'edit_pages',
                'table_name' => 'pages',
                'created_at' => '2021-10-07 01:25:33',
                'updated_at' => '2021-10-07 01:25:33',
            ),
            38 => 
            array (
                'id' => 39,
                'key' => 'add_pages',
                'table_name' => 'pages',
                'created_at' => '2021-10-07 01:25:33',
                'updated_at' => '2021-10-07 01:25:33',
            ),
            39 => 
            array (
                'id' => 40,
                'key' => 'delete_pages',
                'table_name' => 'pages',
                'created_at' => '2021-10-07 01:25:33',
                'updated_at' => '2021-10-07 01:25:33',
            ),
            40 => 
            array (
                'id' => 46,
                'key' => 'browse_tags',
                'table_name' => 'tags',
                'created_at' => '2021-10-07 01:29:07',
                'updated_at' => '2021-10-07 01:29:07',
            ),
            41 => 
            array (
                'id' => 47,
                'key' => 'read_tags',
                'table_name' => 'tags',
                'created_at' => '2021-10-07 01:29:07',
                'updated_at' => '2021-10-07 01:29:07',
            ),
            42 => 
            array (
                'id' => 48,
                'key' => 'edit_tags',
                'table_name' => 'tags',
                'created_at' => '2021-10-07 01:29:07',
                'updated_at' => '2021-10-07 01:29:07',
            ),
            43 => 
            array (
                'id' => 49,
                'key' => 'add_tags',
                'table_name' => 'tags',
                'created_at' => '2021-10-07 01:29:07',
                'updated_at' => '2021-10-07 01:29:07',
            ),
            44 => 
            array (
                'id' => 50,
                'key' => 'delete_tags',
                'table_name' => 'tags',
                'created_at' => '2021-10-07 01:29:07',
                'updated_at' => '2021-10-07 01:29:07',
            ),
            45 => 
            array (
                'id' => 51,
                'key' => 'browse_blogs',
                'table_name' => 'blogs',
                'created_at' => '2021-10-07 01:31:32',
                'updated_at' => '2021-10-07 01:31:32',
            ),
            46 => 
            array (
                'id' => 52,
                'key' => 'read_blogs',
                'table_name' => 'blogs',
                'created_at' => '2021-10-07 01:31:32',
                'updated_at' => '2021-10-07 01:31:32',
            ),
            47 => 
            array (
                'id' => 53,
                'key' => 'edit_blogs',
                'table_name' => 'blogs',
                'created_at' => '2021-10-07 01:31:32',
                'updated_at' => '2021-10-07 01:31:32',
            ),
            48 => 
            array (
                'id' => 54,
                'key' => 'add_blogs',
                'table_name' => 'blogs',
                'created_at' => '2021-10-07 01:31:32',
                'updated_at' => '2021-10-07 01:31:32',
            ),
            49 => 
            array (
                'id' => 55,
                'key' => 'delete_blogs',
                'table_name' => 'blogs',
                'created_at' => '2021-10-07 01:31:32',
                'updated_at' => '2021-10-07 01:31:32',
            ),
            50 => 
            array (
                'id' => 56,
                'key' => 'browse_projects',
                'table_name' => 'projects',
                'created_at' => '2021-10-07 02:28:45',
                'updated_at' => '2021-10-07 02:28:45',
            ),
            51 => 
            array (
                'id' => 57,
                'key' => 'read_projects',
                'table_name' => 'projects',
                'created_at' => '2021-10-07 02:28:45',
                'updated_at' => '2021-10-07 02:28:45',
            ),
            52 => 
            array (
                'id' => 58,
                'key' => 'edit_projects',
                'table_name' => 'projects',
                'created_at' => '2021-10-07 02:28:45',
                'updated_at' => '2021-10-07 02:28:45',
            ),
            53 => 
            array (
                'id' => 59,
                'key' => 'add_projects',
                'table_name' => 'projects',
                'created_at' => '2021-10-07 02:28:45',
                'updated_at' => '2021-10-07 02:28:45',
            ),
            54 => 
            array (
                'id' => 60,
                'key' => 'delete_projects',
                'table_name' => 'projects',
                'created_at' => '2021-10-07 02:28:45',
                'updated_at' => '2021-10-07 02:28:45',
            ),
            55 => 
            array (
                'id' => 61,
                'key' => 'browse_careers',
                'table_name' => 'careers',
                'created_at' => '2021-10-18 03:02:01',
                'updated_at' => '2021-10-18 03:02:01',
            ),
            56 => 
            array (
                'id' => 62,
                'key' => 'read_careers',
                'table_name' => 'careers',
                'created_at' => '2021-10-18 03:02:01',
                'updated_at' => '2021-10-18 03:02:01',
            ),
            57 => 
            array (
                'id' => 63,
                'key' => 'edit_careers',
                'table_name' => 'careers',
                'created_at' => '2021-10-18 03:02:01',
                'updated_at' => '2021-10-18 03:02:01',
            ),
            58 => 
            array (
                'id' => 64,
                'key' => 'add_careers',
                'table_name' => 'careers',
                'created_at' => '2021-10-18 03:02:01',
                'updated_at' => '2021-10-18 03:02:01',
            ),
            59 => 
            array (
                'id' => 65,
                'key' => 'delete_careers',
                'table_name' => 'careers',
                'created_at' => '2021-10-18 03:02:01',
                'updated_at' => '2021-10-18 03:02:01',
            ),
            60 => 
            array (
                'id' => 66,
                'key' => 'browse_apply_form',
                'table_name' => 'apply_form',
                'created_at' => '2021-10-18 03:05:38',
                'updated_at' => '2021-10-18 03:05:38',
            ),
            61 => 
            array (
                'id' => 67,
                'key' => 'read_apply_form',
                'table_name' => 'apply_form',
                'created_at' => '2021-10-18 03:05:38',
                'updated_at' => '2021-10-18 03:05:38',
            ),
            62 => 
            array (
                'id' => 68,
                'key' => 'edit_apply_form',
                'table_name' => 'apply_form',
                'created_at' => '2021-10-18 03:05:38',
                'updated_at' => '2021-10-18 03:05:38',
            ),
            63 => 
            array (
                'id' => 69,
                'key' => 'add_apply_form',
                'table_name' => 'apply_form',
                'created_at' => '2021-10-18 03:05:38',
                'updated_at' => '2021-10-18 03:05:38',
            ),
            64 => 
            array (
                'id' => 70,
                'key' => 'delete_apply_form',
                'table_name' => 'apply_form',
                'created_at' => '2021-10-18 03:05:38',
                'updated_at' => '2021-10-18 03:05:38',
            ),
            65 => 
            array (
                'id' => 71,
                'key' => 'browse_contact_form',
                'table_name' => 'contact_form',
                'created_at' => '2021-10-18 03:07:09',
                'updated_at' => '2021-10-18 03:07:09',
            ),
            66 => 
            array (
                'id' => 72,
                'key' => 'read_contact_form',
                'table_name' => 'contact_form',
                'created_at' => '2021-10-18 03:07:09',
                'updated_at' => '2021-10-18 03:07:09',
            ),
            67 => 
            array (
                'id' => 73,
                'key' => 'edit_contact_form',
                'table_name' => 'contact_form',
                'created_at' => '2021-10-18 03:07:09',
                'updated_at' => '2021-10-18 03:07:09',
            ),
            68 => 
            array (
                'id' => 74,
                'key' => 'add_contact_form',
                'table_name' => 'contact_form',
                'created_at' => '2021-10-18 03:07:09',
                'updated_at' => '2021-10-18 03:07:09',
            ),
            69 => 
            array (
                'id' => 75,
                'key' => 'delete_contact_form',
                'table_name' => 'contact_form',
                'created_at' => '2021-10-18 03:07:09',
                'updated_at' => '2021-10-18 03:07:09',
            ),
        ));
        
        
    }
}