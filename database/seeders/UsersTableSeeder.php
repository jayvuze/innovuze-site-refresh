<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use TCG\Voyager\Models\Role;
use TCG\Voyager\Models\User;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'role_id' => 1,
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$QybBmcg40abMDNbtOnJ5euvm3ndjOJY5RKIdgN5aGiK1VedVws/NG',
                'remember_token' => 'ZfMHbkLRN4IPqQtTEyRK9PNmZzZb1SzjLSKy0K68qtc0X2YWiSPXg4tB9NsH',
                'settings' => NULL,
                'created_at' => '2021-10-07 01:25:32',
                'updated_at' => '2021-10-07 01:25:32',
            ),
        ));
        
        
    }
}