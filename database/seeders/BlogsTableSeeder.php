<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BlogsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('blogs')->delete();
        
        \DB::table('blogs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'DSC-USTP Solutions Week',
                'description' => '<p><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">The student club\'s</span><strong style="box-sizing: border-box; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;Solutions Week 2021</strong><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;event aims to drive every student participant&rsquo;s critical-thinking skills, creativity, and camaraderie by utilizing Google&rsquo;s technologies to create a solution from a problem that coincides with at least one of the 17 Sustainable Development Goals or SDG. It consists of a series of online technical discussions, mentoring sessions, and a pitching event wherein participants gather, learn together and from each other. Experts from different technological backgrounds impart their proficiency and skills to aid in nurturing the solutions created by the participants. In the pitching competition, teams will compete against each other by showcasing their created prototypes and video presentations.</span></p>
<p>&nbsp;</p>
<p><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Day 1 started with a bang. The audience was welcomed with encouraging messages from the University President, Dr.&nbsp;</span><strong style="box-sizing: border-box; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Ambrosio Cultura</strong><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">,&nbsp; DSC Global Lead, Ms.&nbsp;</span><strong style="box-sizing: border-box; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Erica Hanson</strong><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">, Mr.&nbsp;</span><strong style="box-sizing: border-box; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Shad Roi Dela Cruz</strong><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">, and USTP Alumni, Mr.&nbsp;</span><strong style="box-sizing: border-box; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Matthew Maulion</strong><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">. The afternoon session mainly consisted of respectable speakers from different firms discussing Design Thinking, UI/UX Design, and Flutter namely USTP-SHS Principal, Engr.&nbsp;</span><strong style="box-sizing: border-box; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Lloyd Jhon Estampa</strong><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">, Co-founder and Chief Design Officer, Mr.&nbsp;</span><strong style="box-sizing: border-box; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Francis Alturas</strong><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">, and Software Engineer, Engr.&nbsp;</span><strong style="box-sizing: border-box; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Joshua de Guzman</strong><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">.</span></p>
<p>&nbsp;</p>
<p><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">For the third day, it was mainly about each team reaching out to their respective mentors.</span></p>
<p><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;"><img src="http://localhost:8000/storage/blogs/October2021/day-2-photo-op-of-the-audience-with-the-speakers.jpg" alt="" /></span></p>
<p>&nbsp;</p>
<p><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;"><img src="http://localhost:8000/storage/blogs/October2021/day-3-photo-op-with-participants-and-their-mentors.jpg" alt="" /></span></p>
<p>&nbsp;</p>
<p><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">On the fourth day of the said event, each team polished their solutions along with their video presentations that we, the judges, have to choose by the evening. Along with me are Ms.&nbsp;</span><strong style="box-sizing: border-box; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Shoraliah Macalbe</strong><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">, the TBI Manager of CDO B.I.T.E.S, and Engr.</span><strong style="box-sizing: border-box; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Ulrich Uy</strong><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">, the Project Manager of USTP-CDO&rsquo;s Digital Transformation Office. Evening arrived and the judges were then sent with a compilation of video presentations made by the students highlighting their solution&rsquo;s relevance in mitigating the problems they stated. We have to carefully choose and critique which teams proceed to the final round based on their submissions and from those chosen ones, we have to select which has the best video presentation and project prototype. It was supposed to be ten teams, but due to the tie in acquired points after judging, considerations have been made, thus, having fifteen teams progressing to the final round.</span></p>
<p><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;"><img src="http://localhost:8000/storage/blogs/October2021/day-4-photo-after-deliberation-of-teams-that-will-proceed-to-live-pitching1.png" alt="" /></span></p>
<p>&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">The teams that made it to the final round were:</p>
<ul style="box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">
<li style="box-sizing: border-box;">1D Solutions</li>
<li style="box-sizing: border-box;">4sam</li>
<li style="box-sizing: border-box;">Bredrin</li>
<li style="box-sizing: border-box;">ENSURE GOLD</li>
<li style="box-sizing: border-box;">JAML</li>
<li style="box-sizing: border-box;">JRDG</li>
<li style="box-sizing: border-box;">Kwats</li>
<li style="box-sizing: border-box;">Newbie R Us</li>
<li style="box-sizing: border-box;">POKEMON</li>
<li style="box-sizing: border-box;">Robust Pioneers</li>
<li style="box-sizing: border-box;">RUSHED</li>
<li style="box-sizing: border-box;">Techytubbies</li>
<li style="box-sizing: border-box;">TrAZe</li>
<li style="box-sizing: border-box;">USTP CYBERTRONS</li>
<li style="box-sizing: border-box;">ValoTech</li>
</ul>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">The final day arrived and everyone&rsquo;s virtually cheering and rooting for their bets. Each member of the team showed the best version of themselves, prepared their awesome project pitch and prototype, and tried to answer our questions. In my regard, I am highly inclined towards solutions that targets perhaps one massive problem from the given 17 SDGs or more, the solution presented is backed with relevant and reliable data, solutions that specify the technology stack they are about to use and how it is associated with it, and that the solution itself is sustainable.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;"><br style="box-sizing: border-box;" />Some of my picks that best suited some of the aforementioned preferences were:</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;"><strong style="box-sizing: border-box;">TeleDrop by Team TechyTubbies</strong></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">TeleDrop is a mobile application that, according to the team members, utilizes Google Cloud&rsquo;s Vision API and Flutter programming language to map out the groundwater depth of data around a specific area, namely Sitio Pamalihi around Brgy. Pagatpat, that is experiencing water supply problems. This solution helps users of the said area to detect potential groundwater establishment and even safe surface water locations. Furthermore, the team also wanted to integrate Google&rsquo;s ARCore technology to maximize user-friendliness.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;"><strong style="box-sizing: border-box;">DARA by Team POKEMON</strong></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">DARA is computer vision software that detects the attentiveness and mood or energy level of students for each online class or engagements and then creates analytical reports for the teacher to assess the vibe of the class they conducted. In my honest opinion, this solution has a potential to be a startup.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;"><strong style="box-sizing: border-box;">Converse by Team Ensure Gold</strong></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">A sign-language translator application for the speech impaired that utilizes Google Cloud&rsquo;s AutoML Vision to translate the sign commands of the user to text or speech.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">After a whole afternoon of pitches and throws of questions and answers and our final deliberation as judges, the declared winners were:</p>
<ul style="box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">
<li style="box-sizing: border-box;">Team TechyTubbies - Champion</li>
<li style="box-sizing: border-box;">Team USTP CYBERTRONS - 1st Runner Up</li>
<li style="box-sizing: border-box;">Team POKEMON - 2nd Runner Up</li>
</ul>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Team TechyTubbies also won as the best video presentation. To know more about the event and the Developer Student Club - USTP, visit this link:&nbsp;<a class="ui-link" style="box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration-line: none;" href="http://www.facebook.com/dscustp" target="_self">https://www.facebook.com/dscustp</a></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">To "disrupt" in this ever-changing world, old concepts somehow have to adapt to change, out-of-the-box solutions need to be heard, and innovative ideas should be embraced. I&rsquo;d like use a quote from Albert Einstein: &ldquo;Great spirits have always encountered violent opposition from mediocre minds.&rdquo; Indeed, regardless of what others might have to say, bring out your ideas in the open and push on!</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>',
                'image' => 'blogs\\September2021\\Vmi35vN9tDNkoeZdVqQb.jpg',
                'created_at' => '2021-02-27 06:08:00',
                'updated_at' => '2021-10-13 01:50:10',
                'author' => 'February 27, 2021',
            'summary' => 'CAGAYAN DE ORO CITY - The Google Developer Student Club of University of Science and Technology of Southern Philippines - CDO Campus (DSC-USTP) just had its first Solutions Week which was virtually held last February 8 to February 12, 2021.',
                'slug' => 'DSC-USTP-Solutions-Week',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => '7 Years of Innovuze: A New Home',
                'description' => '<p><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">CAGAYAN DE ORO CITY - As</span><strong style="box-sizing: border-box; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;Innovuze&nbsp;Solutions, Inc.&nbsp;</strong><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">celebrates its 7th anniversary and concludes another year, we are about to start in a new home, although not fully operational yet&nbsp;as initially planned&nbsp;because of the ongoing pandemic.&nbsp;We look forward to the right time where everything is ready and everyone in our team&nbsp;can safely collaborate in a conducive environment and convenient location, here at&nbsp;</span><a class="ui-link" style="box-sizing: border-box; color: #337ab7; text-decoration-line: none; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;" href="https://www.projectlupad.com/sm-cdo-downtown-tower-premier-office-spaces-in-the-heart-of-the-city/" target="_self"><strong style="box-sizing: border-box;">SM CDO Downtown Tower</strong></a></p>
<p><span style="color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">.<img src="http://localhost:8000/storage/blogs/October2021/sm-cdo-downtown-tower-sky-garden-innovuze-team-leads.jpg" alt="" /></span></p>
<p><img src="http://localhost:8000/storage/blogs/October2021/sm-cdo-downtown-tower-sky-garden-view-innovuze-team-leads3.jpg" alt="" /></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">About&nbsp;seven years ago, we started as a small web development company with a handful of developers at&nbsp;a humble office near the corner of Capistrano and Macahambus streets&nbsp;in this city. That year, the company was registered as a business in the city, and on May 24, 2013, the company was registered in the Securities&nbsp;and Exchange Commission as a corporation. As we provided more outsourced&nbsp;IT services&nbsp;and as our team grew, we moved later that year to a newer space at&nbsp;<strong style="box-sizing: border-box;">Gateway Tower</strong>&nbsp;in&nbsp;<strong style="box-sizing: border-box;">Limketkai Center</strong>, a lager office&nbsp;for our growing family back then.&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Over the years, a workspace that fits the modern lifestyle at the heart of the city has been part of what we&rsquo;ve been providing to our team when we were still at Gateway Tower, an 8-storey commercial building equipped with backup power and is strategically connected to a premier mall, a hotel, shops, a parking building and the city&rsquo;s state university across it.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">As we continue to grow, we know there comes a time we have to find a new home that can welcome and accommodate more, including everyone in our collaborative team, the new ones who will be&nbsp;<a class="ui-link" style="box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration-line: none;" href="https://www.innovuze.com/careers" target="_self">joining us</a>, interns or on-the-job trainees and work immersion participants&nbsp;from various schools and universities, and also our friends from the vibrant and dynamic tech communities&nbsp;in the city.&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;</p>
<p>&nbsp;</p>
<p><img src="http://localhost:8000/storage/blogs/October2021/sm-cdo-downtown-tower-osmena-side-project-lupad2.png" alt="" /></p>
<p>&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;In this new chapter of our journey, we are stepping a bit higher. As one of the premier office buildings in the city, SM CDO Downtown Tower is the only BPO and multi-use building in the city equipped with a helipad, a sky garden, 6 spacious and high-speed PWD-friendly elevators, backup power, and integrated to&nbsp;<strong style="box-sizing: border-box;">SM CDO Downtown Premier</strong>, the city\'s top-end mall with 5 levels of commercial space, multi-level parking, among other world-class amenities.&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;</p>',
                'image' => 'blogs\\September2021\\fkuzAs66s5UvSfwz148p.png',
                'created_at' => '2021-03-31 06:45:00',
                'updated_at' => '2021-10-13 02:01:37',
                'author' => 'Innovuze solution',
                'summary' => 'CAGAYAN DE ORO CITY - As Innovuze Solutions, Inc. celebrates its 7th anniversary and concludes another year, we are about to start in a new home here at SM CDO Downtown Tower.',
                'slug' => '7-years-of-innovuze-a-new-home',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'My Top 3 Startups at Beacon Demo Day',
            'description' => '<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">CDO B.I.T.E.S., which stands for Cagayan de Oro Business Incubation Technology Entrepreneurship and Startups, is a startup incubator located inside the&nbsp;<strong style="box-sizing: border-box;">University of Science and Technology of Southern Philippines&nbsp;</strong>(USTP) CDO campus that has partnerships with government agencies such as&nbsp;<strong style="box-sizing: border-box;">DOST</strong>,&nbsp;<strong style="box-sizing: border-box;">DTI</strong>&nbsp;and&nbsp;<strong style="box-sizing: border-box;">DICT</strong>. &nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">The&nbsp;<strong style="box-sizing: border-box;">Beacon Demo Day</strong>&nbsp;event is about showing the public the products and services that their incubated startups have created or built.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">There were 12 startups presenting that day and I&rsquo;m picking my Top 3 based on my experience as a software developer taking into consideration which technology they used, how they used it, and the usability of the prototype. I won&rsquo;t comment much on the business aspect since that&rsquo;s not my area of expertise.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;But before I mention my Top 3, let&rsquo;s start with my&nbsp;<strong style="box-sizing: border-box;">honorable mention.</strong></p>
<h2 style="box-sizing: border-box; font-family: \'Source Sans Pro\', sans-serif; font-weight: 500; line-height: 1.1; color: #333333; margin-top: 20px; margin-bottom: 10px; font-size: 30px;">Pic-a-talk</h2>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Pic-A-Talk is a mobile platform for children with special needs. It&rsquo;s targeted for children with speech impairment to communicate and connect with other people.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">With images and icons for everyday objects and actions, a child will select any combination of these images that is converted into a sentence then conveyed to speech using the mobile device&rsquo;s speakers. The images are large, brightly colored, and have larger hit boxes which should be very easy to click on. This is, in their tests, easy to use by special needs children. The images are also customizable. &nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">It should be fairly easy to move this into a larger mobile device like a tablet. They just don&rsquo;t have the backend infrastructure (ie. servers and database) built to support the mobile app.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">You can reach the developers of Pic-a-talk at yumielouise@gmail.com.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;"><strong style="box-sizing: border-box;">My number 3 pick...</strong></p>
<h2 style="box-sizing: border-box; font-family: \'Source Sans Pro\', sans-serif; font-weight: 500; line-height: 1.1; color: #333333; margin-top: 20px; margin-bottom: 10px; font-size: 30px;">Veg E-Fish</h2>
<p><img src="http://localhost:8000/storage/blogs/October2021/limketkai-cdo-bites-beacon-demo-day-stalls-2.jpg" alt="" /></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Veg E-Fish is interesting because it isn&rsquo;t selling an app or service. They are selling an aquaponics kit which they claim to be easy-to-assemble and affordable.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Their demo kit was using a 4 gallon plastic container from water dispensers. They cut and removed the upper part and mounted their kit to it via hoses and tubes. From what I can understand, the kit has a pump, filter and an automated feeder. They plan to make kits in different configurations like a kit equipped with sensors.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">I can see the potential of it for hobbyists and backyard farmers. How they are going to source the kits is a concern at the current time - ie. Covid-19.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Veg E-Fish&rsquo;s email is vegefish.cdo@gmail.com.&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;"><strong style="box-sizing: border-box;">My number 2 pick...</strong></p>
<h2 style="box-sizing: border-box; font-family: \'Source Sans Pro\', sans-serif; font-weight: 500; line-height: 1.1; color: #333333; margin-top: 20px; margin-bottom: 10px; font-size: 30px;">WiTech Innovations</h2>
<p><img src="http://localhost:8000/storage/blogs/October2021/limketkai-cdo-bites-beacon-demo-day-stalls-1.jpg" alt="" /></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">WiTech Innovations is a service. They are a boutique solutions provider focused on automating manual processes.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">During the Demo Day event, they showed us a school entry logging system. The system uses RFID stickers on a student ID cards which are then swiped over a reader. If the RFID sticker is enrolled then the student is allowed entry. It also sends a SMS message to the student&rsquo;s parents. This is also the same when exiting the school.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">The system has several other modules by request such using the RFID system to buy books in the school&rsquo;s bookstore or buying food from the school&rsquo;s canteen - an internal cashless transaction system.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">If you want a sit down meeting with WiTech Innovations you can reach them at tap2.cdo@gmail.com.</p>',
                'image' => 'blogs\\September2021\\rmmKPP8LNOXxrFt8W11H.jpg',
                'created_at' => '2020-03-05 06:50:00',
                'updated_at' => '2021-10-13 02:04:04',
                'author' => 'By J. Ginete',
                'summary' => 'CAGAYAN DE ORO CITY - I got invited to CDO B.I.T.E.S. Beacon Demo Day event last February 26, 2020 at the Limketkai Center Rotunda.',
                'slug' => 'my-top-3-startups-at-beacon-demo-day',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Innovuze OJT: "Nothing Beats Experience"',
                'description' => '<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;OJTs are allowed to perform actual work with the guidance of a mentor. They make the trainees involved in the normal work processes in an actual work-place setting.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">It was my pleasure to have an astounding OJT experience at Innovuze. My hands-on work included:</p>
<ul style="box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">
<li style="box-sizing: border-box;">Firewall and Server Configurations</li>
<li style="box-sizing: border-box;">Maintain and Upgrade Computer Systems</li>
<li style="box-sizing: border-box;">Using Git and GitHub</li>
<li style="box-sizing: border-box;">Computer Networking Training</li>
</ul>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;"><br style="box-sizing: border-box;" />My training allowed me to use existing work tools such as Google Sheets, Trello, and Slack which taught me to be more efficient. I learned different work hacks in my job, which made me ready for my job after graduation.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">I was also oriented to various employment rules and terms which are very useful for my future engagements. I was also trained to follow company policies and learned the importance of confidentiality in handling various company details. The DITS Team provided me with different kinds of tasks and learning tutorials. It was challenging but I enjoyed it. Indeed, there&rsquo;s a lot to learn in a new environment and sometimes doing it is easier than theoretical learning.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;"><img src="http://localhost:8000/storage/blogs/October2021/technical_experience_0_kOLkFuo.jpg" alt="" /></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Indisputably, my training equipped me with technical ideas, knowledge, skills, and familiarity to new updates and development.<br style="box-sizing: border-box;" />Working isn&rsquo;t only focused on regular duties and coping up with tasks. It also includes keeping values, celebrating cultures, and accomplishments they call it&nbsp;<em style="box-sizing: border-box;">Work-Life Balance</em>.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">I am lucky enough to be part of the Innovuze Family Christmas Party 2019. The Christmas Party is the most common event when December comes. It&rsquo;s the season of lights, carols, fireworks, parties, and gifts. It\'s the company\'s annual event to end the year and give convenient gifts to their employees.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">The Christmas Party was held at Grand Caprice Hotel, Limketkai, Lapasan, Cagayan de Oro City participated by the employees along with their family and loved ones.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;"><img src="http://localhost:8000/storage/blogs/October2021/isi_christmas_party.jpg" alt="" /></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">The first game was Singing Bee. Its mechanics are the same as the famous Philippine Game Show - The Singing Bee. Participants of the game were picked in the random manner to present the group. Everyone sings along with the participants as the game goes on. The game totally made my heart jump especially when I joined the contest; feeling excited of what song may appear, and then I slowly heard the tune and the rhythm of the song and realized it was one of my favorite songs in high school, which made me the winner of the first set but in the next set of songs; it was alien&nbsp; to me. And yet, it was the best experience for me with the professionals and being confident with the environment is the key for socialization. The event gave me goosebumps and excitement to see myself becoming like them - an employee&nbsp; in the future.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">The setting, the games, the people, the food and the event, in general amazed me. It was almost the perfect Christmas Party I ever attended; ending the year with a memorable and adorable experience is such a blessing from above.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Competence isn&rsquo;t built in technical abilities alone but also in mental and physical readiness. Innovuze also supports employee sports to enhance camaraderie and teamwork.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">I played with the firm&rsquo;s employees during their regular sport schedules. Personally, I can consider it an extra experience because It keeps me physically fit, mentally ready and socially active. Innovuze definitely encourages work-life balance by sponsoring various games.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;</p>',
                'image' => 'blogs\\September2021\\1r5PGrIIyhpJ4UVaPZ19.jpg',
                'created_at' => '2021-09-29 06:56:00',
                'updated_at' => '2021-10-13 02:05:48',
                'author' => 'K.L. Abao',
                'summary' => 'GINGOOG CITY - Innovuze Solutions, Inc. contributes to student’s development by providing a glimpse of a real-world experience thru their On-the-Job-Training program.',
                'slug' => 'innovuze-ojt-nothing-beats-experience',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Fabulous Innovuze Christmas Party',
                'description' => '<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">With all the preparations, the lights adorning the houses, the children and even adults going around singing Christmas carols, and the cold wind that blows in the evening, it&rsquo;s really hard to miss this special season. And of course, the Christmas Season will always mean countless gatherings and parties, which the Innouvuze company is no stranger to. This year, the company made it extra special.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Last December 13, 2019,&nbsp;<strong style="box-sizing: border-box;">Innovuze Solutions Inc.&nbsp;</strong>held its annual Christmas Party at Grand Caprice Hotel, Limketkai, Lapasan, Cagayan de Oro City. The event was participated by the many teams that make up the company; the Admin Team and HR Staff, the Engineering Team, the Web Development Team, the SEO Team, and the Creatives Team, with some of them bringing along their family and loved ones.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;"><strong style="box-sizing: border-box;">Casino Royale</strong></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">Just like everyone else, Innovuze wanted its night to be extraordinary. The Christmas Party Committees went with one of the most desired themes of the year, a Casino-Themed Party or, simply, the Casino Royale Night. The committee thoroughly prepared for the event, from dress-up, to the table setting, to the decorations, and even to their set of games. Just the concept by itself makes it a classy and remarkable one. The night was filled with all the posh and glamour of a Hotel Casino. Everyone was well-dressed and elegant. Some were dressed to the nine shades of black and red while others are in random colors but in a shining, shimmering, and splendid one.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;"><img src="http://localhost:8000/storage/blogs/October2021/innovuze-christmas-party-2019.jpg" alt="" /></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">The Whole ISI Team was divided into 4 groups: Hearts, Clubs, Diamonds, and Spades. Prior to the said event, each team including the Event Committee were to make their version of a Christmas TV Station-ID. This became one of the highlights of the Party as the groups showcased their artistry and video editing skills.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;"><img src="http://localhost:8000/storage/blogs/October2021/casino-royale-theme-dining-tables.png" alt="" /></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Source Sans Pro\', sans-serif; font-size: 18px;">&nbsp;</p>',
                'image' => 'blogs\\September2021\\rfE2LobWFxIthxMCWXEC.jpg',
                'created_at' => '2021-09-29 07:00:00',
                'updated_at' => '2021-10-13 02:07:09',
                'author' => 'D. Balberan',
                'summary' => 'CAGAYAN DE ORO CITY, Philippines - It is December once again, and for most, especially Filipinos, this only means one thing - Christmas is in front of us.',
                'slug' => 'fabulous-innovuze-christmas-party',
            ),
        ));
        
        
    }
}