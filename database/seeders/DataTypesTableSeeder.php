<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DataTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('data_types')->delete();
        
        \DB::table('data_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'users',
                'slug' => 'users',
                'display_name_singular' => 'User',
                'display_name_plural' => 'Users',
                'icon' => 'voyager-person',
                'model_name' => 'TCG\\Voyager\\Models\\User',
                'policy_name' => 'TCG\\Voyager\\Policies\\UserPolicy',
                'controller' => 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2021-09-17 01:23:55',
                'updated_at' => '2021-09-17 01:23:55',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'menus',
                'slug' => 'menus',
                'display_name_singular' => 'Menu',
                'display_name_plural' => 'Menus',
                'icon' => 'voyager-list',
                'model_name' => 'TCG\\Voyager\\Models\\Menu',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2021-09-17 01:23:55',
                'updated_at' => '2021-09-17 01:23:55',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'roles',
                'slug' => 'roles',
                'display_name_singular' => 'Role',
                'display_name_plural' => 'Roles',
                'icon' => 'voyager-lock',
                'model_name' => 'TCG\\Voyager\\Models\\Role',
                'policy_name' => NULL,
                'controller' => 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2021-09-17 01:23:55',
                'updated_at' => '2021-09-17 01:23:55',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'categories',
                'slug' => 'categories',
                'display_name_singular' => 'Category',
                'display_name_plural' => 'Categories',
                'icon' => 'voyager-categories',
                'model_name' => 'TCG\\Voyager\\Models\\Category',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2021-09-17 01:23:56',
                'updated_at' => '2021-09-17 01:23:56',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'posts',
                'slug' => 'posts',
                'display_name_singular' => 'Post',
                'display_name_plural' => 'Posts',
                'icon' => 'voyager-news',
                'model_name' => 'TCG\\Voyager\\Models\\Post',
                'policy_name' => 'TCG\\Voyager\\Policies\\PostPolicy',
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2021-09-17 01:23:56',
                'updated_at' => '2021-09-17 01:23:56',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'pages',
                'slug' => 'pages',
                'display_name_singular' => 'Page',
                'display_name_plural' => 'Pages',
                'icon' => 'voyager-file-text',
                'model_name' => 'TCG\\Voyager\\Models\\Page',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2021-09-17 01:23:56',
                'updated_at' => '2021-09-17 01:23:56',
            ),
            6 => 
            array (
                'id' => 13,
                'name' => 'tags',
                'slug' => 'tags',
                'display_name_singular' => 'Tag',
                'display_name_plural' => 'Tags',
                'icon' => 'voyager-tag',
                'model_name' => 'App\\Models\\Tags',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}',
                'created_at' => '2021-10-07 01:29:07',
                'updated_at' => '2021-10-07 01:29:07',
            ),
            7 => 
            array (
                'id' => 14,
                'name' => 'blogs',
                'slug' => 'blogs',
                'display_name_singular' => 'Blog',
                'display_name_plural' => 'Blogs',
                'icon' => 'voyager-file-text',
                'model_name' => 'App\\Models\\Blogs',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}',
                'created_at' => '2021-10-07 01:31:32',
                'updated_at' => '2021-10-13 03:40:43',
            ),
            8 => 
            array (
                'id' => 15,
                'name' => 'projects',
                'slug' => 'projects',
                'display_name_singular' => 'Project',
                'display_name_plural' => 'Projects',
                'icon' => 'voyager-archive',
                'model_name' => 'App\\Models\\Projects',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}',
                'created_at' => '2021-10-07 02:28:45',
                'updated_at' => '2021-10-19 01:32:00',
            ),
            9 => 
            array (
                'id' => 16,
                'name' => 'careers',
                'slug' => 'careers',
                'display_name_singular' => 'Career',
                'display_name_plural' => 'Careers',
                'icon' => 'voyager-people',
                'model_name' => 'App\\Models\\Career',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}',
                'created_at' => '2021-10-18 03:02:01',
                'updated_at' => '2021-10-18 03:02:01',
            ),
            10 => 
            array (
                'id' => 17,
                'name' => 'apply_form',
                'slug' => 'apply-form',
                'display_name_singular' => 'Apply Form',
                'display_name_plural' => 'Apply Forms',
                'icon' => 'voyager-credit-cards',
                'model_name' => 'App\\Models\\ApplyForm',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}',
                'created_at' => '2021-10-18 03:05:38',
                'updated_at' => '2021-10-18 03:11:19',
            ),
            11 => 
            array (
                'id' => 18,
                'name' => 'contact_form',
                'slug' => 'contact-form',
                'display_name_singular' => 'Contact Form',
                'display_name_plural' => 'Contact Forms',
                'icon' => 'voyager-paper-plane',
                'model_name' => 'App\\Models\\ContactForm',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}',
                'created_at' => '2021-10-18 03:07:09',
                'updated_at' => '2021-10-19 01:52:48',
            ),
        ));
        
        
    }
}