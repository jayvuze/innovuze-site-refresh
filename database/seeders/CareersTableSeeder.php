<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CareersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('careers')->delete();
        
        \DB::table('careers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Senior Web Developer',
                'desc' => 'Yes, a developer. A dummy text, please.',
                'career_reqs' => 'Req 1, Req 2, Req 3',
                'image' => 'storage\\careers\\resume-icon.png',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Software QA Officer',
                'desc' => 'Yes, a developer. A dummy text, please.',
                'career_reqs' => 'Req 1, Req 2, Req 3',
                'image' => 'storage\\careers\\resume-icon.png',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Scrum Master',
                'desc' => 'Yes, a developer. A dummy text, please.',
                'career_reqs' => 'Req 1, Req 2, Req 3',
                'image' => 'storage\\careers\\resume-icon.png',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}