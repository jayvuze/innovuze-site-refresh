<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tags')->delete();
        
        \DB::table('tags')->insert(array (
            0 => 
            array (
                'id' => 1,
                'tag_name' => 'party',
                'created_at' => '2021-10-07 02:13:46',
                'updated_at' => '2021-10-07 02:13:46',
            ),
            1 => 
            array (
                'id' => 2,
                'tag_name' => 'innovuze',
                'created_at' => '2021-10-07 02:13:50',
                'updated_at' => '2021-10-07 02:13:50',
            ),
            2 => 
            array (
                'id' => 3,
                'tag_name' => 'ojt',
                'created_at' => '2021-10-07 02:14:10',
                'updated_at' => '2021-10-07 02:14:10',
            ),
            3 => 
            array (
                'id' => 4,
                'tag_name' => 'ustp',
                'created_at' => '2021-10-07 02:24:45',
                'updated_at' => '2021-10-07 02:24:45',
            ),
            4 => 
            array (
                'id' => 5,
                'tag_name' => 'training',
                'created_at' => '2021-10-07 02:24:49',
                'updated_at' => '2021-10-07 02:24:49',
            ),
        ));
        
        
    }
}