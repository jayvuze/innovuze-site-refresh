<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ContactFormTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('contact_form')->delete();
        
        \DB::table('contact_form')->insert(array (
            0 => 
            array (
                'id' => 1,
                'email' => 'sehan15474@smuvaj.com',
                'name' => 'Joo',
                'subject' => 'ENHYPEN',
                'msg_body' => 'dimension de5',
                'created_at' => '2021-10-15 05:34:56',
                'updated_at' => '2021-10-15 05:34:56',
            ),
        ));
        
        
    }
}