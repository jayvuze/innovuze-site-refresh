<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('projects')->delete();
        
        \DB::table('projects')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Innovuze Solutions Tracker',
                'url' => 'https://tracker.innovuze.com/login',
                'created_at' => '2021-09-22 01:30:00',
                'updated_at' => '2021-09-22 01:32:32',
                'images' => 'projects\\September2021\\ViH8H3i1Haauv4SycwM9.png',
                'description' => 'This website serves as a tracker for Innovuze Solutions employees',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Calabrian Childrens Foundation',
                'url' => 'https://www.calabrian-kids.org/',
                'created_at' => '2021-09-23 02:37:56',
                'updated_at' => '2021-09-23 02:37:56',
                'images' => 'projects\\September2021\\wcWBMDubOyRNYLGUWzYs.png',
                'description' => 'The website is for Calabrian Childrens Foundation',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'House of Joy',
                'url' => 'https://www.houseofjoycdo.org/',
                'created_at' => '2021-09-23 02:39:00',
                'updated_at' => '2021-10-21 03:33:31',
                'images' => 'projects\\September2021\\4oXtQaZaEd0U1miI62gn.png',
                'description' => '“House of Joy” is a social concern and non-profit charitable home that administer shelter and care of the children.',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Gardentronics',
                'url' => 'https://gardentronic.com/',
                'created_at' => '2021-09-23 02:41:00',
                'updated_at' => '2021-10-21 03:34:13',
                'images' => 'projects\\September2021\\J2W0L1mYWXdBEsuv2KTn.png',
                'description' => 'GET INSPIRED. SAVE IDEAS. FIND PROFESSIONALS. GET THE GARDEN THAT YOU HAVE ALWAYS WANTED.',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'EEWeb',
                'url' => 'https://www.eeweb.com/',
                'created_at' => '2021-10-20 15:36:00',
                'updated_at' => '2021-10-21 03:30:00',
                'images' => 'projects\\October2021\\d8xvM2UZneWHojZQilui.png',
                'description' => 'The voice of record for the electronics industry.',
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'Schematics',
                'url' => 'https://www.schematics.com',
                'created_at' => '2021-10-20 15:36:00',
                'updated_at' => '2021-10-21 03:28:50',
                'images' => 'projects\\October2021\\8bydksaxS7uSKl1wfX5F.jpg',
                'description' => 'Sign up to design, share, and collaborate on your next project—big or small.',
            ),
            6 => 
            array (
                'id' => 7,
                'title' => 'PCBWeb',
                'url' => 'https://www.pcbweb.com/',
                'created_at' => '2021-10-20 15:37:00',
                'updated_at' => '2021-10-21 03:22:13',
                'images' => 'projects\\October2021\\E8iJbRbiny1il8c0zovF.jpg',
                'description' => 'PCBWeb is a free CAD application for designing and manufacturing electronics hardware.  Download',
            ),
            7 => 
            array (
                'id' => 8,
                'title' => 'PartSim',
                'url' => 'https://www.partsim.com',
                'created_at' => '2021-10-20 15:37:00',
                'updated_at' => '2021-10-21 03:21:33',
                'images' => 'projects\\October2021\\IUScF8MFdnfmijMTJbjp.jpg',
                'description' => 'PartSim is a free and easy to use circuit simulator that runs in your web browser.',
            ),
            8 => 
            array (
                'id' => 10,
                'title' => 'amaya view',
                'url' => 'https://www.amayaview.com',
                'created_at' => '2021-10-20 15:39:00',
                'updated_at' => '2021-10-21 03:53:08',
                'images' => 'projects\\October2021\\3w7ANfggWP2xg87x9LCl.png',
                'description' => 'Amaya View completes the city’s water to air tourist experience.',
            ),
        ));
        
        
    }
}