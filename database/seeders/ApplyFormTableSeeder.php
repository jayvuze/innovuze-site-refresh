<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ApplyFormTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('apply_form')->delete();
        
        \DB::table('apply_form')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Sorry jilleo',
                'email' => 'doubleo@minjoo.da',
                'contact_num' => '095',
                'rsme_file' => NULL,
                'cov_lett' => 'Hee',
                'created_at' => '2021-09-27 14:25:55',
                'updated_at' => '2021-09-27 14:25:55',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'ju',
                'email' => 'won@cold.com',
                'contact_num' => '78',
                'rsme_file' => NULL,
                'cov_lett' => 'uiui',
                'created_at' => '2021-10-14 08:48:19',
                'updated_at' => '2021-10-14 08:48:19',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'uu',
                'email' => 'ee@h.com',
                'contact_num' => '665',
                'rsme_file' => NULL,
                'cov_lett' => 'fdkfj',
                'created_at' => '2021-10-14 09:53:11',
                'updated_at' => '2021-10-14 09:53:11',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Jessi',
                'email' => 'sehan15474@smuvaj.com',
                'contact_num' => '678',
                'rsme_file' => NULL,
                'cov_lett' => 'huhu',
                'created_at' => '2021-10-15 05:40:01',
                'updated_at' => '2021-10-15 05:40:01',
            ),
        ));
        
        
    }
}