<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;

class MenuItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('menu_items')->delete();
        
        \DB::table('menu_items')->insert(array (
            0 => 
            array (
                'id' => 1,
                'menu_id' => 1,
                'title' => 'Dashboard',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-boat',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 1,
                'created_at' => '2021-10-01 11:03:32',
                'updated_at' => '2021-10-01 11:03:32',
                'route' => 'voyager.dashboard',
                'parameters' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'menu_id' => 1,
                'title' => 'Media',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-images',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 4,
                'created_at' => '2021-10-01 11:03:32',
                'updated_at' => '2021-10-07 02:37:37',
                'route' => 'voyager.media.index',
                'parameters' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'menu_id' => 1,
                'title' => 'Users',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-person',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 3,
                'created_at' => '2021-10-01 11:03:32',
                'updated_at' => '2021-10-01 11:03:32',
                'route' => 'voyager.users.index',
                'parameters' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'menu_id' => 1,
                'title' => 'Roles',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-lock',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 2,
                'created_at' => '2021-10-01 11:03:32',
                'updated_at' => '2021-10-01 11:03:32',
                'route' => 'voyager.roles.index',
                'parameters' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'menu_id' => 1,
                'title' => 'Tools',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-tools',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 11,
                'created_at' => '2021-10-01 11:03:32',
                'updated_at' => '2021-10-18 03:16:31',
                'route' => NULL,
                'parameters' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'menu_id' => 1,
                'title' => 'Menu Builder',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-list',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 1,
                'created_at' => '2021-10-01 11:03:32',
                'updated_at' => '2021-10-07 02:37:37',
                'route' => 'voyager.menus.index',
                'parameters' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'menu_id' => 1,
                'title' => 'Database',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-data',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 2,
                'created_at' => '2021-10-01 11:03:32',
                'updated_at' => '2021-10-07 02:37:37',
                'route' => 'voyager.database.index',
                'parameters' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'menu_id' => 1,
                'title' => 'Compass',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-compass',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 3,
                'created_at' => '2021-10-01 11:03:32',
                'updated_at' => '2021-10-07 02:37:37',
                'route' => 'voyager.compass.index',
                'parameters' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'menu_id' => 1,
                'title' => 'BREAD',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-bread',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 4,
                'created_at' => '2021-10-01 11:03:32',
                'updated_at' => '2021-10-07 02:37:37',
                'route' => 'voyager.bread.index',
                'parameters' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'menu_id' => 1,
                'title' => 'Settings',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-settings',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 12,
                'created_at' => '2021-10-01 11:03:32',
                'updated_at' => '2021-10-18 03:16:31',
                'route' => 'voyager.settings.index',
                'parameters' => NULL,
            ),
            10 => 
            array (
                'id' => 15,
                'menu_id' => 1,
                'title' => 'Tags',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-tag',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 6,
                'created_at' => '2021-10-01 11:07:29',
                'updated_at' => '2021-10-18 03:16:29',
                'route' => 'voyager.tags.index',
                'parameters' => NULL,
            ),
            11 => 
            array (
                'id' => 16,
                'menu_id' => 1,
                'title' => 'Blogs',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-file-text',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 7,
                'created_at' => '2021-10-07 01:31:32',
                'updated_at' => '2021-10-18 03:16:29',
                'route' => 'voyager.blogs.index',
                'parameters' => NULL,
            ),
            12 => 
            array (
                'id' => 17,
                'menu_id' => 1,
                'title' => 'Projects',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-archive',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 5,
                'created_at' => '2021-10-07 02:28:45',
                'updated_at' => '2021-10-19 01:32:56',
                'route' => 'voyager.projects.index',
                'parameters' => 'null',
            ),
            13 => 
            array (
                'id' => 18,
                'menu_id' => 1,
                'title' => 'Careers',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-people',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 8,
                'created_at' => '2021-10-18 03:02:01',
                'updated_at' => '2021-10-18 03:16:29',
                'route' => 'voyager.careers.index',
                'parameters' => NULL,
            ),
            14 => 
            array (
                'id' => 19,
                'menu_id' => 1,
                'title' => 'Apply Forms',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-credit-cards',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 9,
                'created_at' => '2021-10-18 03:05:38',
                'updated_at' => '2021-10-18 03:16:31',
                'route' => 'voyager.apply-form.index',
                'parameters' => NULL,
            ),
            15 => 
            array (
                'id' => 20,
                'menu_id' => 1,
                'title' => 'Contact Forms',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-paper-plane',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 10,
                'created_at' => '2021-10-18 03:07:09',
                'updated_at' => '2021-10-19 01:53:09',
                'route' => 'voyager.contact-form.index',
                'parameters' => 'null',
            ),
        ));
        
        
    }
}