<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCareersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Create database table for storing Careers job information
         */
        Schema::create('careers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 250)->nullable();
            $table->string('desc', 500)->nullable();
            $table->string('career_reqs', 500)->nullable();
            $table->string('image', 250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careers');
    }
}
