
@extends('master')

<style>
  .hero {
	 background-position: 50% 50%;
	 background-repeat: no-repeat;
	 background-size: cover;
	 backface-visibility: hidden;
	 height: 650px;
	 overflow: hidden;
	 position: relative;
	 width: 100%;
}
 @media screen and (min-width: 640px) {
	 .hero {
		 background-position: 50% 0;
	}
}
 .hero:after {
	 background-color: rgba(62, 135, 66, 0.5);
	 bottom: 0;
	 content: '';
	 left: -2000%;
	 position: absolute;
	 right: -2000%;
	 top: 0;
	 z-index: 1;
}
 .hero-container {
   margin-top: 80px;
	 box-sizing: border-box;
	 height: auto;
	 padding: 30px 50px;
	 position: relative;
	 z-index: 2;
	 -webkit-transition-timing-function: cubic-bezier(0.7, 0, 0.3, 1);
	 transition-timing-function: cubic-bezier(0.7, 0, 0.3, 1);
	 -webkit-transition-duration: 1.2s;
	 transition-duration: 1.2s;
}
 @media screen and (min-width: 640px) {
	 .hero-container {
		 height: 100%;
	}
}
 .hero-container p {
	 color: white;
	 font-size: 13px;
	 font-weight: bold;
	 letter-spacing: 6px;
	 opacity: 0.8;
	 text-transform: uppercase;
	 text-align: center;
}
 .hero-title {
	 color: white;
	 font-size: 17px;
	 font-weight: 50;
	 letter-spacing: 1px;
	 margin: 5px 0;
	 text-align: center;
	 width: 100%;
}
 @media screen and (min-width: 640px) {
	 .hero-title {
		 font-size: 10vw;
	}
}
}
 
 
  </style>
@section('content')
    <main>
      
<section class="hero image-as-background" style="background-image: url('{{ Voyager::image($blogs->image) }}')">
  <div class="hero-container">
    <h1 class="hero-title">{{ $blogs->title }}</h1>
    <p >{{ $blogs->created_at }}</p>
  </section>

        <section class="news-detail section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-10 mx-auto">
                      <br>
                        <span>Posted by:{{ $blogs->author }}</span>
                        <br>
                        <br>

                        <p >{{ $blogs->summary }}</p>

                        <p >{!!$blogs->description!!}</p>
                        <h4>Tags:</h4>
                        @foreach($blogs->tags as $tag)
                        <a href="/blogs/tags/{{ $tag->tag_name }}">{{ $tag->tag_name }}</a>
                        @endforeach
                    </div>

                </div>
            </div>
        </section>

        <section class="related-news section-padding">
            <div class="container">
                <div class="row">



                </div>
            </div>
        </section>

        {{-- <div class="modal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Modal title</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"></span>
              </button>
            </div>
            <div class="modal-body">
              <p>Modal body text goes here.</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary">Save changes</button>
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div> --}}

    </main>

@endsection
