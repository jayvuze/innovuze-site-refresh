@extends('master')

@section('content')
<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/tiny-slider.css">

<!-- HeroImage starts Here -->
    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="caption">
                        <h2>Innovuze Solutions Inc.</h2>
                        <div class="line-dec"></div>
                        <p>Innovuze Solutions, Inc. is a Web Development and Information Technology company that is
                            providing websites,
                            software, digital content, marketing, sales and back-office support, and other IT services.
                            We are a dynamic technology and services company with a great working environment and positive
                            culture.
                            We are a registered business in Cagayan de Oro City, Philippines since 2013 and also a member of
                            the Cagayan de Oro ICT Business Council.
                        </p>
                        <div class="main1">
                            <a href="/#contact">Contact Us </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- HeroImage ends Here -->

    <!-- about-page starts Here -->
    <div class="about-page">
        <div class="container "  id="about-us">
            <div class="row">
                <div class="col-md-6">
                    <div class="left-image">
                        <img src="images/about.gif" alt="About Us Icon">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="right-content">
                        <h4>Innovuze Solutions Information</h4>
                        <p>Our office is now located at <strong>SM CDO Downtown Tower</strong>, the <em>only</em> building
                            in <strong>Cagayan de Oro City</strong> that
                            is equipped with a <u>helipad</u>, a <u>sky garden</u>, <strong>spacious and high-speed
                                PWD-friendly</strong> <u>elevators</u>, <u>backup
                                power</u>, and <em>integrated</em> to SM CDO Downtown Premier, the city's <em>top-end</em>
                            mall with <strong>5</strong> levels of
                            commercial space, multi-level parking, among other world-class amenities. </p>
                        <br>
                        <p><strong>Address:</strong> SM Downtown Premier, Claro M Recto 9000 Cagayan de Oro City Northern
                            Mindanao </p>
                        <br>
                        <p><strong>Phone:</strong> (088) 324 0454</p>
                        <br>
                        <div id=container>
                            WE
                            <div id=flip>
                                <div>
                                    <div>innovate</div>
                                </div>
                                <div>
                                    <div>solve</div>
                                </div>
                                <div>
                                    <div>inspire</div>
                                </div>
                            </div>
                            Innovuze<br>
                            solutions inc
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <p style="text-align:center; margin:auto"><img src="images/innovuze_op.png" alt="Innovuze"></p>
    <!-- about-page ends Here -->

    <!-- Career Starts Here -->
    <div class="featured-items">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <p id="career">
                            <img src="images/title-icn-careers-home.png" alt="Careers Icon"></p>
                        <br>
                        <br>
                        <h4>Want to become part of our family? We're hiring!</h4>
                    </div>
                </div>



                @foreach ($careers as $career)


                    <div class="col-xs-12 col-md-12 col-lg-4">

                        <div class="card">
                            <div class="card-in">
                                <div class="career">
                                    <div class="careerHomeHeader">
                                        <img src="{{ $career->image }}" alt="Career Job Image">
                                        <h4>{{ $career->title }}</h4>
                                        <br>
                                    </div>


                                    <div class="careerHomeDesc">
                                        <p>{{ $career->desc }}</p>

                                    </div>

                                    <div class="more-details">
                                        <a href="/careers/{{ $career->title }}" target="_self" class="ui-link">
                                            <div style="margin-left: 50px" class="btn btn-primary" role="button"
                                                title="{{ $career->title }}">More details</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <br><br>

    <div class="main">
        <a href="/careers">More info </a>
    </div>
<!-- Careers ends Here -->



    <!-- Services starts Here -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <p id="services">
                        <img src="images/title-icn-services-green.png" alt="Services Icon"></p>
                    <br><br>
                    <h4>Let technology help your business grow!</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container1 ">
        <div class="image-grid">
            <img class="image-grid-col-2 image-grid-row-2" src="images/devices.gif" alt="Services">

            <div class="image1">
                <img class="image-grid-col-2 image-grid-row-2" style="text-align: center;"
                    src="images/services-digital-media.png" alt="Digital Media">
                    <h4>Digital Media</h4>
                <div class="image__overlay image__overlay--blur">
                    <div class="image__title">Digital Media Service is an online service provider that sells access to digital library of content such as films, software, games, images, literature, etc. </div>
                </div>
            </div>
            <div class="image2">
                <img class="image-grid-col-2 image-grid-row-2" src="images/services-cloud-infrastructure-management.png"
                    alt="Cloud Infrastucrture ">
                    <h4>Cloud Infrastructure Management</h4>
                <div class="image__overlay image__overlay--blur">
                    <div class="image__title">We are evolving into the Cloud in an effort to optimize performance, stability, and flexibility within IT infrastructure.</div>
                </div>
            </div>
            <div class="image3">
                <img class="image-grid-col-2 image-grid-row-2" src="images/services-mobile-development.png"
                    alt="mobile development">
                    <h4>Mobile Development</h4>
                <div class="image__overlay image__overlay--blur">
                    <div class="image__title">Mobile app development services cover end-to-end development of mobile apps, from business analysis and UI/UX design to mobile application testing and deployment or online market publication.</div>
                </div>
            </div>
            <div class="image4">
                <img class="image-grid-col-2 image-grid-row-2" src="images/services-web-development.png"
                    alt="web development">
                    <h4>Web development</h4>
                <div class="image__overlay image__overlay--blur">
                    <div class="image__title">Web development services are used to design, build, support, and evolve all types of web-based software.</div>
                </div>
            </div>
        </div>
    </div>
    <!-- Services ends Here -->

    <!--Projects starts here-->
    <div class="container"  id="projects">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <p id="projects"><img src="images/title-icn-projects.png" alt="Projects Icon"></p>
                    <br>
                    <br>
                    <h4>Innovuze Solutions, Inc. is working together with
                        industry leaders in developing websites, applications, and online communities which include the
                        following:</h4>
                    <br>
                    <br>
                </div>

                <div class="img-slider">
                    @foreach ($projects as $projects)
                    <div class="slide">
                        <a href="{{ $projects->url }}">
                            <img src="{{ Voyager::image($projects->images) }}" alt="1">
                        </a>
                    </div>
                    @endforeach
                    @foreach ($count as $projects)
                    <div class="slide">
                        <a href="{{ $projects->url }}">
                            <img src="{{ Voyager::image($projects->images) }}" alt="1">
                        </a>
                    </div>
                    @endforeach
                    
                    
                    <div class="navigation">
                        <div class="btn2 active"></div>
                        @foreach ($projects as $count)
                        <div class="btn2"></div>
                        <div class="btn2"></div>
                        
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>

    <div class="main projects">
        <a href="/project">More info </a>
    </div>
    <script type="text/javascript">
        var slides = document.querySelectorAll('.slide');
        var btns = document.querySelectorAll('.btn2');
        let currentSlide = 1;

        // Javascript for image slider manual navigation
        var manualNav = function(manual) {
            slides.forEach((slide) => {
                slide.classList.remove('active');

                btns.forEach((btn2) => {
                    btn2.classList.remove('active');
                });
            });

            slides[manual].classList.add('active');
            btns[manual].classList.add('active');
        }

        btns.forEach((btn2, i) => {
            btn2.addEventListener("click", () => {
                manualNav(i);
                currentSlide = i;
            });
        });

        // Javascript for image slider autoplay navigation
        var repeat = function(activeClass) {
            let active = document.getElementsByClassName('active');
            let i = 1;

            var repeater = () => {
                setTimeout(function() {
                    [...active].forEach((activeSlide) => {
                        activeSlide.classList.remove('active');
                    });

                    slides[i].classList.add('active');
                    btns[i].classList.add('active');
                    i++;

                    if (slides.length == i) {
                        i = 0;
                    }
                    if (i >= slides.length) {
                        return;
                    }
                    repeater();
                }, 10000);
            }
            repeater();
        }
        repeat();
    </script>
    <!-- projects ends Here -->
    
    <!--blogs starts here-->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <p id="blog"><img src="images/title-icn-blog-home.png"
                            alt="Blog Icon"></p>
                    <br>
                    <br>
                    <h4>Get updated with the latest events happening within the company.</h4>
                </div>
            </div>
        </div>
        <div class="slider-container">
            <ul class="controls" id="customize-controls" aria-label="Carousel Navigation" tabindex="0">
                <li class="prev" data-controls="prev"  tabindex="-1">
                    <i class="fa fa-angle-left fa-5x"></i>
                </li>
                <li class="next" data-controls="next" tabindex="-1">
                    <i class="fa fa-angle-right fa-5x"></i>
                </li>
            </ul>
            <div class="my-slider">
                @foreach ($blogs as $blog)
                    <a href="/blogs/{{ $blog->slug }}">
                        <div class="slider-item">
                            <div class="slidercard">
                                <img style="height:200px;" src="{{ Voyager::image($blog->image) }}" alt="blogs">
                                <h2>{{ $blog->title }}</h2>
                                <p class="card_description">{{ $blog->summary }}</p>
                                <p>Posted on {{ $blog->created_at }}</p>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
    <div class="main">
        <a href="/blogs">More info </a>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-polyfills/0.1.43/polyfill.js"></script>
    <script>
        const slider = tns({
            container: '.my-slider',
            loop: true,
            items: 1,
            slideBy: 'page',
            nav: false,
            autoplay: true,
            speed: 400,
            autoplayButtonOutput: false,
            mouseDrag: true,

            controlsContainer: "#customize-controls",
            responsive: {
                640: {
                    items: 1,
                },
                768: {
                    items: 2,
                },

                1000: {
                    items: 3,
                }
                
            }

        });
    </script>




    <!-- Contact Starts Here -->
    <div class="contact-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <p id="contact-us">
                            <img src="images/title-icn-green-contact-us.png" alt="Contact Us Icon">
                            <br><br>
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="map">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15784.617047803635!2d124.6547634!3d8.4843776!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x940a9ac2aee772dd!2sSM%20CDO%20Downtown%20Premier!5e0!3m2!1sen!2sph!4v1631740519660!5m2!1sen!2sph"
                            width="500" height="500" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="right-content">


                        <!-- ALERT MESSAGE -->
                        @if (Session::has('success'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                                {{ Session::get('success') }}
                            </div>
                        @elseif ($errors->any())
                            <div class="alert alert-danger">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </div>
                        @endif


                        <div class="container">
                            {!! Form::open([
                                'id' => 'contact',
                                'route' => 'contact.store',
                                'enctype' => 'multipart/form-data',
                            ]) !!}

                            <div class="row">
                                <div class="col-md-6">


                                    {{ Form::text('name', null, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Your name...',
                                        'id' => 'name',
                                        'required' => '',
                                    ]) }}

                                </div>
                                <div class="col-md-6">

                                    {{ Form::text('email', null, [
                                        'class' => 'form-control',
                                        'id' => 'email',
                                        'placeholder' => 'Your email...',
                                        'required' => '',
                                    ]) }}

                                </div>
                                <div class="col-md-12">

                                    {{ Form::text('subject', null, [
                                        'class' => 'form-control',
                                        'id' => 'subject',
                                        'placeholder' => 'Subject...',
                                        'required' => '',
                                    ]) }}


                                </div>
                                <div class="col-md-12">

                                    {{ Form::textarea('msg_body', null, [
                                        'class' => 'form-control',
                                        'style' => 'height: 163px; resize: none;',
                                        'id' => 'message',
                                        'placeholder' => 'Your message...',
                                        'rows' => 7,
                                        'required',
                                    ]) }}

                                </div>

                                <div class="col-md-12">
                                    <div class="form-group mt-4 mb-4">
                                        <div class="captcha">
                                            <span>{!! captcha_img() !!}</span>
                                            <button type="button" class="btn btn-danger" id="reload">
                                                &#x21bb;
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">

                                    {{ Form::text('captcha', null, [
                                        'class' => 'form-control',
                                        'id' => 'captcha',
                                        'placeholder' => 'Enter Captcha...',
                                        'required' => '',
                                    ]) }}

                                </div>

                                <div class="col-md-12">
                                    <fieldset>
                                        <button type="submit" id="form-submit" class="button">Send Message</button>
                                    </fieldset>
                                </div>
                                <div class="col-md-12">
                                    <div class="share">
                                        <h6>You can also keep in touch on:
                                            <span>
                                                <a href="https://www.facebook.com/innovuzesolutions">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                                <a href="https://www.linkedin.com/company/innovuze-solutions-inc-">
                                                    <i class="fa fa-linkedin"></i>
                                                </a>
                                            </span>
                                        </h6>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
