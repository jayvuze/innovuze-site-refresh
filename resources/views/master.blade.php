<!DOCTYPE html>

<html>
<head>

    <title>Innovuze Solutions Inc. Website</title>
    <link rel="shortcut icon" href="/images/logo-trans.png">
    <meta charset="utf-8">
    
    <meta name="viewport" content="#343434 , width=device-width, initial-scale=1, shrink-to-fit=no,">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

    

    
    <link href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    


    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="{{ URL::asset('css/fontawesome.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
    






</head>

<body>
    

    @include('partials.nav')

    @include('partials.side')

    @yield('content')

    @include('partials.footer')


</body>

</html>
