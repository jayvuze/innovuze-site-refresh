@extends('master')


@section('content')
    <br>
    <br>
    <div class="container">
        <div class="section-heading">
            <div class="caption">
                <h2><strong>Projects</strong></h2>
                <div class="line-dec"></div>
            </div>
        </div>
    </div>
    <ul class="honeycomb">
        @foreach ($projects as $projects)
        
            <li class="honeycomb-cell">
                <a href="{{ $projects->url }}">
                <img class="honeycomb-cell__image" src="{{ Voyager::image($projects->images) }}">
                <div class="honeycomb-cell__title">{{ $projects->title }}
                    <p>{{ $projects->description }}</p>
                </div>
            </a>
            </li>
        
        @endforeach
    </ul>


@endsection
