@extends('master')


@section('content')
<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">

<section class="careerMainHeader">
    <img
        src="images/title-icn-careers-home.png" alt="Careers">
</section>

        <br><br>

            @foreach($careers as $career)
        
                <div class="card border-primary mb-3" style="max-width: 100%;">
                <div class="card-header"><h1>{{ $career->title }}</h1></div>


                    <div class="card-body">
                        {{ $career->desc }}
                    </div>

                    <div class="card-footer">
                        @include('partials.applyFormModal')
                    </div>

                </div>
            
            @endforeach

            

        {{-- </div>
    </div> --}}

@endsection