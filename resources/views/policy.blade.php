

@extends('master')


@section('content')
<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">

    <main>

        <section class="hero image-as-background" style="background-image: url('{{ asset('images/privacy.jpg') }}')">
            <div class="hero-container">
              <h1 class="hero-title"><strong>Privacy</strong></h1>
            </section>


        <section class="news-detail section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-10 mx-auto">
                        <br><br>
                        <h2>Privacy Policy</h2>

                        <p>
                            This site or application is created, managed, and/or owned by Innovuze Solutions Inc.
                            (henceforth
                            referred as “ISI” or “we” or "us"). ISI respects the rights of its users to personal data
                            privacy and is committed to protect the data it collects. This privacy policy explains how we
                            use, store and process any personal information we collect about you when you use this site or
                            application in accordance with Republic Act 10173 or the Data Privacy Act of 2012.
                        </p>
                        <h4>
                            What information we collect about you
                        </h4>
                        <p>
                            In general, we may collect personal information from you when you voluntarily provide it to us,
                            such as when you submit application for job vacancies with us, send inquiries about our
                            services, make use of our services, and provide feedbacks though our contact form. Such
                            information may include but may not be limited to:
                        </p>
                            
                        <ul>
                            <li>Complete name</li>
                            <li>Home address</li>
                            <li>Email address</li>
                            <li>Employment information</li>
                            <li>Educational attainment </li>
                            <li>Email address</li>
                        </ul>
                            
                        
                        <h4>Do we use cookies?</h4>
                        <p>
                            Cookies are small text files stored on your computer that collects visitor information and
                            behaviour to help the website keep track of your visits and activity. It may also keep a record
                            of your login information. This may be helpful to our website, however, we do not use cookies.
                        </p>
                        <h4>Why your information is processed and how we use it</h4>
                        <p>
                            The personal information you provide to us will solely be used for documentation and processing
                            purposes. Personally identifiable information collected from you shall only be used for the
                            intended and permitted purposes such as for job application, talent pooling, respond to your
                            inquiries or feedbacks, and informing you about our site, services, products and/or job
                            opportunities. We may also inform you about training programs or community events that we
                            sponsor or promote. These programs or events may be organized by parties that may or may not be
                            related to us in any way. We do not warrant any programs, activities, products, services, or
                            goods by these other parties. Your transactions with these parties are solely between you and
                            these parties. You agree that we shall not be held liable for any loss or damage incurred as a
                            result of any such transaction or with your participation on the events and programs of these
                            other parties. Prior consent shall be required for any other purposes.
                        </p>
                        <h4>How we protect your information</h4>
                        <p>
                            We follow generally accepted industry standards to safeguard personal information and other data
                            provided to us. Data collected are stored in secure servers and includes regular monitoring for
                            security breaches and other reasonable measures to protect your personal information. Employees
                            who are involved or are responsible for the storage and processing of these data are also
                            bounded by the company’s confidentiality or non-disclosure agreement.

                            We do not share your personal information to third parties such as advertisers without your
                            permission or consent.

                            Personal information collected from you will be kept confidential unless we are required by law
                            or court order to provide such information for legal purposes.

                            If, in any case, personal data breaches occur, you will be notified without undue delay after we
                            become aware of it.
                        </p>
                        <h4>Your rights</h4>
                        <p>You are entitled to:</p>
                        <ul>
                            <li>Access your personal information and have it amended or deleted;</li>
                            <li>Obtain a copy of your personal data in an electronic or structured format;</li>
                            <li>Suspend or withdraw your consent to the processing of your personal information at any time
                                by contacting us using the details below;</li>
                        </ul>
                        <h4>Changes to our Privacy Policy</h4>
                        <p>This privacy policy will be kept under regular review and may be periodically updated. Changes
                            will be posted on this page.</p>
                        <h4>Questions or feedback</h4>
                        <p>If you have any questions, comments or suggestions regarding our privacy policy, please email us
                            at privacy@innovuze.com or by using the contact details below:</p>
                        <br>
                        <dl>
                            <dt>Data Protection Officer</dt>
                            <dt>Innovuze Solutions Inc.</dt>
                            <dt>8th Level, Gateway Tower, Limketkai Center</dt>
                            <dt>Lapasan, Cagayan de Oro City</dt>
                            <dt>Philippines 9000</dt>
                        </dl>
                        <br><br>
                    </div>

                </div>
            </div>
        </section>


    </main>
@endsection
