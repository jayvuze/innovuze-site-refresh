

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ApplyFormModal">
        Apply Now
    </button>

    <!-- Modal -->
    <div class="modal fade" id="ApplyFormModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                @if(session('status'))
                    <div class="alert alert-dismissible alert-success">
                        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                        {{ session('status') }}
                    </div>
                @elseif ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
                @endif


                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Apply Now</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>

                <div class="modal-body">

                    {!! Form::open([
                        'url' => 'form-submit',
                        'enctype' => 'multipart/form-data',
                        'files' => true,
                        'method' => 'POST'
                        ]) 
                    !!}
                    @csrf
                        <div class="form-group">
                            {{ Form::label('name', null, [
                                'class' => 'form-label mt-4'
                                ]) 
                            }}

                            {{ Form::text('name', null, [
                                'class'=>'form-control',
                                'name' => 'name',
                                'required',
                                'id' => 'name'
                                ]) 
                            }}
                            
                        </div>

                        <div class="form-group">
                            {{ Form::label('email_address', null, [
                                'class' => 'form-label mt-4'
                                ]) 
                            }}

                            {{ Form::text('email_address', null, [
                                'class'=>'form-control',
                                'name' => 'email',
                                'id' => 'email_address',
                                'required'
                                ]) 
                            }}

                        </div>

                        <div class="form-group">
                            {{ Form::label('contact_number', null, [
                                'class' => 'form-label mt-4'
                                ]) 
                            }}

                            {{ Form::text('contact_number', null, [
                                'class'=>'form-control',
                                'name' => 'contact_num',
                                'id' => 'contact_number',
                                'aria-describedby' => 'cntctNumMsg',
                                'required'
                                ]) 
                            }}

                            <small id="cntctNumMsg" class="form-text text-muted">Enter your 11-digit mobile number.</small>

                        </div>

                        <div class="form-group">
                            {{ Form::label('upload_resume', null, [   
                                'class' => 'form-label mt-4'
                                ]) 
                            }}

                            <br>

                            {{ Form::file('rsme_file', null, [
                                    'class' => 'form-control',
                                    'id' => 'upload_resume',
                                    'accept' => '.pdf',
                                    'aria-describedby' => 'emailHelp',
                                    'required'
                                ]) 
                            }}

                            <small id="fileSizeMsg" class="form-text text-muted">Upload file in PDF only. (Max Filesize: 10MB)</small>

                        </div>

                        <div class="form-group">
                            {{ Form::label('cover_letter', null, [
                                'class' => 'form-label mt-4'
                                ]) 
                            }}

                            {{ Form::textarea('cover_letter', null, [
                                'class' => 'form-control', 
                                'name' => 'cov_lett',
                                'rows' => 3,
                                'id' => 'cover_letter',
                                'required'
                                ]) 
                            }}

                        </div>

                        <div class="form-group mt-4 mb-4">
                            <div class="captcha">
                                <span>{!! captcha_img() !!}</span>
                                <button type="button" class="btn btn-danger" class="reload" id="reload">
                                    &#x21bb;
                                </button>
                            </div>
                        </div>
      
                        <div class="form-group">
    
                            {{ Form::text('captcha', null, [
                                'class'=> 'form-control',
                                'id' => 'captcha',
                                'placeholder' => 'Enter Captcha...',
                                'required' => ''
                                ]) 
                            }}
                            
                        </div>
                        
                        <br>
                        <br>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Submit</button> 
                        </div>

                    {!! Form::close() !!}
                </div>


            </div>
        </div>
    </div>