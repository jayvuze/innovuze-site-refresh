<div class="footer1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="logo">
                    <img src={{ asset('images/hero-img.png') }} alt="">
                </div>
            </div>
            <div class="col-md-12">
                <div class="footer1-menu">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="/privacy-policy">Privacy Policy</a></li>
                        <li><a href="/terms-and-conditions">Terms and Conditions</a></li>
                        <li><a href="/#contact">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-12">
                <div class="social-icons">
                    <ul>
                        <li><a href="https://www.facebook.com/innovuzesolutions"><i  style="line-height: 30px;" class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/innovuze-solutions-inc-"><i style="line-height: 30px;" class="fa fa-linkedin" ></i></a></li>
                        <li><a href="https://misorjobs1.rssing.com/chan-13794687/all_p148.html"><i  style="line-height: 30px;" class="fa fa-rss" ></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer Ends Here -->




<!-- Bootstrap core JavaScript -->
<script src="{{ URL::asset("jquery/jquery.min.js") }}"></script>
<script src="{{ URL::asset("bootstrap/js/bootstrap.bundle.min.js") }}"></script>

<script>
    cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
    function clearField(t) { //declaring the array outside of the
        if (!cleared[t.id]) { // function makes it static and global
            cleared[t.id] = 1; // you could use true and false, but that's more typing
            t.value = ''; // with more chance of typos
            t.style.color = '#fff';
        }
    }
</script>

<script type="text/javascript">
    $('#reload').click(function () {
        $.ajax({
            type: 'GET',
            url: 'captcha-refresh',
            success: function (data) {
                $(".captcha span").html(data.captcha);
            }
        });
    });

</script>
