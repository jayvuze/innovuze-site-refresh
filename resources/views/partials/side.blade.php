<div class="box">
    <div class="single-box" style="background: #8BC63F">
      <a href="https://www.facebook.com/innovuzesolutions"><p>Get updated on<i class="fa fa-facebook"></i></p></a>
    </div>
    
    <div class="single-box" style="background: #a8d869">
      <a href="/#contact"><p>Contact us on<i class="fa fa-envelope"></i></p></a>
    </div>
    
    
    <div class="single-box" style="background: #698840">
      <a href="https://www.linkedin.com/company/innovuze-solutions-inc-"><p>Follow us on<i class="fa fa-linkedin"></i></p></a>
    </div>
      
  </div>