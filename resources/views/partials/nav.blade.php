<!-- Navigation Header starts here -->
<header>
    <a href="/#" class="brand"><img src={{ asset('images/main-logo-sm.png') }} alt ="logo"></a>
    <div class="menu">
        <div class="btn">
            <i class="fa fa-times close-btn"></i>
        </div>
        <a href="/#">Home</a>
        <a href="/#about-us">About Us</a>
        <a href="/#career">Career</a>
        <a href="/#services">Services</a>
        <a href="/#projects">Projects</a>
        <a href="/#blog">Blog</a>
        <a href="/#contact">Contact</a>

    </div>
    <div class="btn">
        <i class="fa fa-bars menu-btn"></i>
    </div>
</header>
<script type="text/javascript">
    //javascript for navigation bar effect on scroll
    window.addEventListener("scroll", function(){
      var header = document.querySelector("header");
      header.classList.toggle('sticky', window.scrollY > 0);
    });

    //javascript for responsive navigation sidebar menu
    var menu = document.querySelector('.menu');
    var menuBtn = document.querySelector('.menu-btn');
    var closeBtn = document.querySelector('.close-btn');

    menuBtn.addEventListener("click", () => {
      menu.classList.add('active');
    });

    closeBtn.addEventListener("click", () => {
      menu.classList.remove('active');
    });
    </script>
