@extends('master')

@section('content')
    <br>
    <br>
    <div class="container">
        <div class="section-heading">
            <div class="caption">
                <h2><strong>Blogs</strong></h2>
                <div class="line-dec"></div>
            </div>
        </div>
    </div>
    <!-- Sidebar starts Here -->
    <div class="side">
        <form class="search-container" action={{ '/blogs' }} method="GET">
            <div class="search__container">
                <p class="search__title">
                    What can we do for you?
                </p>
                <input class="search__input" name="search" type="text" placeholder="Search">
            </div>
        </form>
        <br>
        <br>
        <div class="sidenav">
            <div class="_content">
                <h4>Archive</h4>
                <dl class="list">
                    @foreach ($archives as $date)
                        <dd>
                            <a href="/blogs/?month={{ $date['month'] }}&amp;year={{ $date['year'] }}">
                                {{ $date['month'] . ' ' . $date['year'] }}</a>
                        </dd>
                    @endforeach
                </dl>
            </div>
            <div class="__content">
                <h4>Tags</h4>
                <dl class="list">
                    @foreach ($tags as $tag)
                        <dd>
                            <a href="/blogs/tags/{{ $tag->tag_name }}">
                                {{ $tag->tag_name }}
                            </a>
                        </dd>
                    @endforeach
                </dl>
            </div>
        </div>

        <div class="container3">

            <div class="card3">
                <h2>Facebook</h2>
                <i class="fa fa-arrow-right"></i>
                <p>visit us on</p>
                <div class="pic"></div>
                <a href="https://www.facebook.com/innovuzesolutions">
                    <button>
                    </button>
                </a>
            </div>

            <div class="card3 card4">
                <h2>Contact</h2>
                <i class="fa fa-arrow-right"></i>
                <p>contact us on</p>
                <div class="pic"></div>
                <a href="/#contact">
                    <button>
                    </button>
                </a>
            </div>
        </div>
    </div>
    <!-- Sidebar ends Here -->

    <!-- Blogs starts Here -->
    <div class="container">

        @forelse($blogs as $blog)
        @empty
            <p> No results found for search: <strong>{{ request()->query('search') }}</strong></p>
            <img src={{ asset('images/analyze.gif') }} alt="can't find">
        @endforelse
        @foreach ($blogs as $blog)
            <a href="/blogs/{{ $blog->slug }}">
                <div class="blog-post">
                    <div class="blog-post_img">
                        <img src="{{ Voyager::image($blog->image) }}" alt="blog">
                    </div>
                    <div class="blog-post_content">
                        <h4>{{ $blog->title }}</h4>
                        <p>{{ $blog->summary }}</p>
                        <h6>Posted on {{ $blog->created_at }}</h6>

                    </div>
                </div>
            </a>
        @endforeach
    </div>
    <br>
    <div class="container1">
        {{ $blogs->appends(['search' => request()->query('search')])->links() }}
    </div>
    <br>
    <!-- Blogs ends Here -->

    <style>
        .w-5 {
            height: 35px;
            text-align: center;
            display: center;
            margin: 0 auto;
        }

    </style>














@endsection
