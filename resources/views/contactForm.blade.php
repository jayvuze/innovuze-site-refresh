<!DOCTYPE html>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootswatch@5.1.1/dist/vapor/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>


    <div class="card border-primary mb-3" style="max-width: 20rem;">
        <div class="card-header"><h1>Contact Us</h1></div>

        @if(Session::has('success'))
            <div class="alert alert-dismissible alert-success">
                <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                {{ Session::get('success') }}
                <!-- <strong>Well done!</strong> You successfully read <a href="#" class="alert-link">this important alert message</a>.  -->
            </div>
        @endif

            <div class="card-body">
                {{-- <!-- <form method='POST' action="{{ route('contact.send') }}" enctype="multipart/form-data"> --> --}}
                {!! Form::open([
                    'route' => 'contact.store',
                    'enctype' => 'multipart/form-data'
                    ]) 
                !!}
                    <div class="form-group">
                        {{ Form::label('Email address', null, [
                            'for' => 'exampleInputEmail1',
                            'class' => 'form-label mt-4'
                            ]) 
                        }}

                        {{ Form::text('email', null, [
                            'class'=>'form-control',
                            'id' => 'exampleInputEmail1',
                            'aria-describedby' => 'emailHelp'
                            ]) 
                        }}

                        {{-- <label for="exampleInputEmail1" class="form-label mt-4">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"> --}}
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>

                    <div class="form-group">
                        {{ Form::label('Name', null, [
                            'for' => 'inputDefault', 
                            'class' => 'col-form-label mt-4'
                            ]) 
                        }}

                        {{ Form::text('name', null, [
                            'class'=>'form-control',
                            'id' => 'inputDefault'
                            ]) 
                        }}

                        {{-- <label class="col-form-label mt-4" for="inputDefault">Name</label>
                        <input type="text" class="form-control" id="inputDefault"> --}}
                    </div>

                    <div class="form-group">
                        {{ Form::label('Email Subject', null, [
                            'for' => 'inputDefault',
                            'class' => 'col-form-label mt-4'
                            ]) 
                        }}

                        {{ Form::text('subject', null, [
                                'class' => 'form-control',
                                'id' => 'inputDefault'
                            ]) 
                        }}
                        {{-- <label class="col-form-label mt-4" for="inputDefault">E-mail Subject</label>
                        <input type="text" class="form-control" id="inputDefault"> --}}
                    </div>

                    <div class="form-group">
                        {{ Form::label('Message', null, [
                            'for' => 'exampleTextarea', 
                            'class' => 'form-label mt-4'
                            ]) 
                        }}

                        {{ Form::textarea('msg_body', null, [
                            'class' => 'form-control', 
                            'id' => 'exampleTextarea', 
                            'rows' => 3
                            ]) 
                        }}

                        {{-- <label for="exampleTextarea" class="form-label mt-4">Message</label>
                        <textarea class="form-control" id="exampleTextarea" rows="3"></textarea> --}}
                    </div>
                    
                    <br>
                    <br>

                    <button type="submit" class="btn btn-primary">Submit</button>

                    {!! Form::close() !!}
                
            </div>

    </div>


    

    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script> -->
</body>
</html>