@extends('master')

@section('content')
<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">

<section class="perJob">
    <img
        src="../images/title-icn-careers-home.png" alt="Careers">
</section>

    <br><br>
        
    <div class="card border-primary mb-3" style="max-width: 100%;">
    <div class="card-header"><h1>{{ $careers->title }}</h1></div>


        <div class="card-body">
            {{ $careers->desc }}
        </div>

        <div class="card-footer">
            @include('partials.applyFormModal')
        </div>

    </div>


@endsection