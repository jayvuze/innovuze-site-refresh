<?php

namespace App\Http\Controllers;

use App\Mail\ApplyFormMail;
use App\Models\ContactForm;
use App\Mail\ContactMail;
use App\Models\ApplyForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FormController extends Controller
{
    public function contact()
    {
        return view('contactForm');
    }

    public function contactFormPost(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'name' => 'required',
            'subject' => 'required',
            'msg_body' => 'required',
            'captcha' => 'required|captcha'
        ]);

        ContactForm::create($request->all());

        $formDet = [
            'email' => $request->email,
            'name' => $request->name,
            'subject' => $request->subject,
            'msg_body' => $request->msg_body,
        ];

        Mail::to($request->email)->send(new ContactMail($formDet));

        return back()->with('success', 'Thank you for contacting!');
    }



    public function applyFormPost(Request $req)
    {
        $this->validate($req, [
            'name' => 'required',
            'email' => 'required|email',
            'contact_num' => 'required|digits:11',
            'cov_lett' => 'required',
            'rsme_file' => 'required|mimes:pdf|max:10240',
            'captcha' => 'required|captcha'
        ]);

        $applicant = new ApplyForm;
        $applicant->name = $req->input('name');
        $applicant->email = $req->input('email');
        $applicant->contact_num = $req->input('contact_num');
        $applicant->cov_lett = $req->input('cov_lett');

        if($req->hasfile('rsme_file'))
        {
            $fileNm = time() . '_' . $req->file('rsme_file')->getClientOriginalName();
            $filePath = $req->file('rsme_file')->storeAs('uploads/applicants', $fileNm, 'public');

            $applicant->rsme_file = '/storage/' . $filePath;
        }

        $applicant->save();

        $formDet = [
            'name' => $req->name,
            'email' => $req->email,
            'contact_num' => $req->contact_num,
            'cov_lett' => $req->cov_lett,
            'rsme_file' => $req->rsme_file,
        ];

        Mail::to($req->email)->send(new ApplyFormMail($formDet));
        
        return redirect('/#contact')->with('status', "Thank you for applying!");
    }

    public function reloadCaptcha()
    {
        return response()->json(['captcha' => captcha_img()]);
    }
}
