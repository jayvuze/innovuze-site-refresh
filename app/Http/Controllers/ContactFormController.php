<?php

namespace App\Http\Controllers;

use App\Models\ContactForm;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

/**
 * controller for Contact Us form on homepage
 */
class ContactFormController extends Controller
{
    /**
     * render Contact Us form on homepage
     * 
     * 
     * @param string|null $view
     * 
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function contact()
    {
        return view('contactForm');
    }

    /**
     * Contact Form validation and response mailing
     * 
     * 
     * @param \Illuminate\Http\Request $request
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function contactFormPost(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'name' => 'required',
            'subject' => 'required',
            'msg_body' => 'required',
            'captcha' => 'required|captcha',
        ]);

        ContactForm::create($request->all());

        $formDet = [
            'email' => $request->email,
            'name' => $request->name,
            'subject' => $request->subject,
            'msg_body' => $request->msg_body,
        ];

        Mail::to('jonclarkpanerxugs2001@gmail.com')->send(new ContactMail($formDet));

        return back()->with('success', 'Thank you for contacting!');
    }
}
