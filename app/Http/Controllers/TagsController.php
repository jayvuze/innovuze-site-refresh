<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tags;
use App\Models\Blogs;

class TagsController extends Controller
{
    public function index(Tags $tags)
    {
        $archives = Blogs::selectRaw('year(created_at) year, monthname(created_at) month,count(*) published')
        ->groupBy('year','month')
        ->orderByRaw('min(created_at)desc')
        ->get()
        ->toArray();
        
        $blogs = $tags->blogs;
        $tags = Tags::paginate(3);
        
        return view('blogTags',compact('blogs','archives','tags'));
    }
}
