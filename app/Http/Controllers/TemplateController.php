<?php

namespace App\Http\Controllers;

use App\Models\Blogs;
use App\Models\Career;
use App\Models\Projects;
use App\Models\Tags;
use Carbon\Carbon;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Post;

class TemplateController extends Controller
{
    public function index()
    {
        $blogs = Blogs::orderBy('created_at','DESC')->simplePaginate(6);
        $careers = Career::get();
        $projects = Projects::get();
        $count = Projects::orderBy('created_at','ASC')->paginate(2);
        return view('home', compact('careers','projects','blogs','count'));
    }

    public function blogs()
    {
        $blogs = Blogs::paginate(3);
        $featured = Blogs::get();
        $archives = Blogs::selectRaw('year(created_at) year, monthname(created_at) month,count(*) published')
            ->groupBy('year', 'month')
            ->orderByRaw('min(created_at)desc')
            ->get()
            ->toArray();
        $search = request()->query('search');
        if ($search) {
            $blogs = Blogs::where('title', 'LIKE', "%{$search}%")->simplePaginate(3);
        }

        if ($month = request('month')) {
            if ($year = request('year')) {
                $blogs = Blogs::whereYear('created_at', '=', $year)
                    ->whereMonth('created_at', Carbon::parse($month)->month)
                    ->paginate(3);
            }
        }

        $tags = Tags::get();
        return view('blog', compact('blogs', 'archives', 'tags', 'featured'));
    }

    /**
     * Create the component instance.
     *
     * @param  string  $slug
     */
    public function singleBlog($slug)
    {
        /**
         * Retrieve the first result of the query however,
         *if no result is found, an Illuminate\Database\Eloquent\ModelNotFoundException
         *will be thrown
         */
        $blogs = Blogs::where('slug', '=', $slug)->firstOrFail();
        return view('single', compact('blogs'));
    }

    public function project()
    {
        $projects = Projects::get();
        return view('project', compact('projects'));
    }

    /**
     * render Careers page/view for all job postings
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function careersRend()
    {
        $careers = Career::get();
        return view('careers', compact('careers'));
    }

    /**
     * render view for inidivudal career job postings
     *
     * @param mixed $careerTitle
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function eachCareerPos($careerTitle)
    {
        $careers = Career::where('title', '=', $careerTitle)->firstOrFail();

        return view('perJob', compact('careers'));
    }

    public function privacy()
    {

        return view('policy');
    }

    public function terms()
    {

        return view('how');
    }

}
