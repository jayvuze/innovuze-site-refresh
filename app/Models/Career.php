<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * create database model/class, declare db table name
 */
class Career extends Model
{
    use HasFactory;

    protected $table = 'careers';
}
