<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * create class, declare variable and array for database table and fillable attributes of Careers Application Form
 */
class ApplyForm extends Model
{
    use HasFactory;
    
    protected $table = 'apply_form'; // tbl
    public $fillable = ['name', 'email', 'contact_num', 'rsme_file', 'cov_lett']; // fillable; 
}
