<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    use HasFactory;
    public function getRouteKeyName()
    {
        return 'tag_name';
    }
    public function blogs()
    {
        return $this->belongstoMany(Blogs::class,'blogs_tags');
    }
}
