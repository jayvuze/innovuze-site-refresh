<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * create class, declare variable and array for database table and fillable attributes of Contact Us form
 */
class ContactForm extends Model
{
    use HasFactory;

    protected $table = 'contact_form'; // tbl
    public $fillable = ['email', 'name', 'subject', 'msg_body']; // fillable; 
}
